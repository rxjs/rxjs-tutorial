import * as Rx from 'rxjs/Rx';

export class Part02CreatingObservablesModule {
  constructor() {
    // console.log('Part02CreatingObservablesModule loaded....');
  }

  public createSubject(): Rx.Subject<{}> {
    let o = new Rx.Subject();
    return o;
  }

  public createObservable(): Rx.Observable<string> {
    let o = Rx.Observable.create(observer => {
      observer.next('puppy');
      setTimeout(() => {
        observer.next('kitty');
        setTimeout(() => observer.next('cub'), 1000);
      }, 1000);
    }) as Rx.Observable<string>;  // ???
    return o;
  }
}

// Testing
function testDrive() {
  console.log('testDrive() called.');

  let m = new Part02CreatingObservablesModule();

  let o1 = m.createSubject();
  o1.subscribe(value => console.log(value));
  o1.next('apple');
  o1.next('orange');

  let o2 = m.createObservable();
  o2.subscribe(value => console.log(value));

}
testDrive();
