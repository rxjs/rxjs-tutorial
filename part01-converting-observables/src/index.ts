
import * as Rx from 'rxjs/Rx';

export class Part01ConvertingObservablesModule {
  constructor() {
    // console.log('Part01ConvertingObservablesModule loaded....');
  }

  public convertValues(arg1: string, arg2: string): Rx.Observable<string> {
    let o = Rx.Observable.of(arg1, arg2);
    return o;
  }

  public convertArray(args: string[]): Rx.Observable<string> {
    let o = Rx.Observable.from(args);
    return o;
  }

  public convertRange(min: number, max: number): Rx.Observable<number> {
    let o = Rx.Observable.range(min, max);
    return o;
  }

  // ???
  public convertTimer(interval: number): Rx.Observable<number> {
    let o = Rx.Observable.timer(interval);
    return o;
  }
}

// Testing
function testDrive() {
  console.log('testDrive() called.');

  let m = new Part01ConvertingObservablesModule();

  let o1 = m.convertValues('apple', 'orange');
  o1.subscribe(value => console.log(value));

  let arr = ['a', 'b', 'c'];
  let o2 = m.convertArray(arr);
  o2.subscribe(value => console.log(value));

  let o3 = m.convertRange(1, 10);
  o3.subscribe(value => console.log(value));

  // tbd: What is this supposed to do?
  let o4 = m.convertTimer(1200);
  o4.subscribe(value => console.log(value));
  
}
testDrive();
