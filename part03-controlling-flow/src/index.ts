import * as Rx from 'rxjs/Rx';

export class Part03ControllingFlowModule {
  constructor() {
    // console.log('Part03ControllingFlowModule loaded....');
  }

  public createObservable(): Rx.Observable<string> {
    let o = Rx.Observable.create(observer => {
      observer.next('apple');
      setTimeout(() => {
        observer.next('orange');
        setTimeout(() => {
          observer.next('pear')
          setTimeout(() => {
            observer.next('banana')
          }, 1500);
        }, 1000);
      }, 500);
    }) as Rx.Observable<string>;  // ???
    return o;
  }
}

// Testing
function testDrive() {
  console.log('testDrive() called.');

  let m = new Part03ControllingFlowModule();

  let o1 = m.createObservable();
  o1.subscribe(value => console.log('1: ' + value));

  let o2 = m.createObservable();
  o2.filter(str => str.length > 4)
  .map(str => '  ' + str + str.length)
  .subscribe(value => console.log('2: ' + value));

  let o3 = m.createObservable();
  o3.delay(750)
  .map(str => '   ' + str + str.length)
  .subscribe(value => console.log('3: ' + value));

  let o4 = m.createObservable();
  o4.throttleTime(750)
  .map(str => '    ' + str + str.length)
  .subscribe(value => console.log('4: ' + value));

  let o5 = m.createObservable();
  o5.debounceTime(750)
  .map(str => '     ' + str + str.length)
  .subscribe(value => console.log('5: ' + value));

  let o6 = m.createObservable();
  o6.take(2)
  .map(str => '      ' + str + str.length)
  .subscribe(value => console.log('6: ' + value));

}
testDrive();
